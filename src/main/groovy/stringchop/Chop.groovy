package stringchop

import groovy.transform.CompileStatic

import java.util.regex.Matcher
import java.util.regex.Pattern


// TODO: split to indexable refs? gobble forward by number? chopNth?

// a fluent pattern string "chopper" that allows us to extract data from a string doing multiple complicated operations
// in a sequence while avoiding having to declare intermediate variables, whcih in the case of substring gets annoying.
// to make this truly fluid/fluent/functional, we'll need more method support such as replacement and others.
@CompileStatic
class Chop {
    String head = ''
    String mid = ''
    String tail = ''
    List<String> accumulator = []
    
    static Pattern whitespace = Pattern.compile('\\s+')

    static Chop init(String s, List<String> accumulator = []) {
        Chop chop = new Chop(accumulator: accumulator)
        chop.tail = s
        return chop
    }

    static String[] parts(String str, String substr) { init(str).chop(substr).parts() }

    Chop chop(String substr) {
        String src = tail
        int idx = src ? src.indexOf(substr) : -1
        if (idx >= 0) {
            Chop chop = new Chop(accumulator: accumulator)
            chop.head = src.substring(0, idx)
            chop.mid = substr
            chop.tail = src.substring(idx + substr.length())
            return chop
        } else {
            // if not found, then the entire string is used up/chopen
            return new Chop(head: src, accumulator: accumulator)
        }
    }

    // only chop if it is there, aka we preserve the state if the lookahead/if is not satisfied
    Chop chopIf(String substr) {
        tail.indexOf(substr) == -1 ? this : chop(substr)
    }

    // uses lastIndexOf rather than indexOf
    Chop gobble(String substr) {
        String src = tail
        int idx = src.lastIndexOf(substr)
        if (idx > 0) {
            Chop chop = new Chop(accumulator:accumulator)
            chop.head = src.substring(0, idx)
            chop.mid = substr
            chop.tail = src.substring(idx + substr.length())
            return chop
        } else {
            return new Chop(head: src)
        }
    }

    // chop until first regex match
    Chop chopRx(String regex) {
        Pattern p = Pattern.compile(regex)
        chopRx(p)
    }

    //
    Chop chopRx(Pattern p) {
        String src = tail
        Matcher m = p.matcher(src)
        boolean found = m.find()
        if (found) {
            int matchStartIdx = m.start()
            int matchEndIdx = m.end()
            Chop chop = new Chop(accumulator:accumulator)
            chop.head = src.substring(0, matchStartIdx)
            chop.mid = src.substring(matchStartIdx, matchEndIdx)
            chop.tail = src.substring(matchEndIdx)
            return chop
        } else {
            new Chop(head: src)
        }
    }

    // only chop if it is there, aka we preserve the state if the lookahead/if is not satisfied
    Chop chopRxIf(String p) {
        chopRxIf(Pattern.compile(p))
    }

    Chop chopRxIf(Pattern p) {
        p.matcher(tail).find() ? chopRx(p) : this
    }

    Chop chopToken() {
        chopRx(whitespace)
    }

    Chop head() { init(head, accumulator) }

    Chop mid() { init(mid, accumulator) }

    Chop tail() { init(tail, accumulator) }

    String[] parts() { [head, mid, tail] as String[] }
    
    String getHeadMid() { head + mid }

    String getMidTail() { mid + tail }
    
    Chop accHead() { accumulator.add(head); return this }

    Chop accMid() { accumulator.add(mid); return this }

    Chop accTail() { accumulator.add(tail); return this }

    String accumulated() { accumulator?.join('') }

}
