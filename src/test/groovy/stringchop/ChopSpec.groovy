package stringchop

import spock.lang.Specification
import spock.lang.Unroll

class ChopSpec extends Specification {

    @Unroll
    void 'substring #test'() {
        when:
        String[] result = Chop.init(test).chop(bite).parts()

        then:
        expected == result

        where:
        test             | bite   | expected
        'dir too short1' | 'too ' | ['dir ', 'too ', 'short1']
        'dir too short2' | ' '    | ['dir', ' ', 'too short2']
    }

    @Unroll
    void 'regex #test'() {
        when:
        String[] result = Chop.init(test).chopRx(bite).parts()

        then:
        expected == result

        where:
        test             | bite    | expected
        'dir too short'  | '\\s+'  | ['dir', ' ', 'too short']
        'dir too short1' | '\\s+t' | ['dir', ' t', 'oo short1']
        'dir too short2' | 'oops'  | ['dir too short2','','']
    }

    String s3SSTable = "s3://superapp-backupdir" +
            "/prod" +
            "/Shard_A_south" +
            "/rackA-East--10.0.94.40" +
            "/system_auth" +
            "/complex_table_name" +
            "/1539753679957--2018-10-17_05.21.19--INC-1539753604050/__mnt__ebs1__data/lb-1-big-CompressionInfo.db"

    void 's3test'() {
        when:
        String result = Chop.init(s3SSTable).chop("s3://").chop("/system_auth/complex_table_name/").tail().chop('/').head().gobble('--').tail

        then:
        result == "INC-1539753604050"
    }

}
