# chop
A fluent-style string chopping utility for extracting data from complex strings composed of multiple parts using regexs or substrings. Often can reduce a sea of substrings, indexOfs, and other calls to one-liners.

This library attempts to make extracting info from complex encoded strings a more pleasant and fluent experience. Although many of these operations can be done by complicated regexes, those are often very hard to figure out and debug.

Call chop.init(thestring). Then call chop(substring) to find the first encountered string. The Chop object return has the head, mid, and tail. 

head is the part of the string preceding the matching value. 
tail is the part of the string following the mathcing value.
mid is the part of the string that matched the "chop" value, aka the part of the string that was chopped out. Becomes even more useful for chopRx()

Example:

    String holyUglyFileBatman = "MANIFEST::{dev--europe--eu-west}__10.13.152.233__(mightygod.blaster)__1554870002553--2019-04-10_04.20.02--dailybackup.json"

    String IP = Chop.init(holyUglyFileBatman).chop('__').chop('__').head
    String epochmillis = Chop.init(holyUglyFileBatman).chop('__').chop('__').chop('__').chopRx('[0-9]+').mid
    String fileprefix = Chop.init(holyUglyFileBatman).chop('::').head